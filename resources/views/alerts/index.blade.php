@extends('layouts.app')
@section('content')
<script src="/plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/plugins/jquery-datatable/jquery.dataTables.js" type="text/javascript"></script>
<link rel="stylesheet" href="/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css">
<script src="/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>

  <section class="content">
      <div class="container-fluid">
          <div class="block-header">
              <h2>ALERTAS / EMERGENCIAS</h2>
          </div>

              <div id="map" class="col-md-12">
                  <div id="divMap" style="width:100%;height:350px;border:1px solid gray;margin-bottom:20px;"></div>
              </div>


              <div class="col-md-12">


              <table class="datatable table table-hover" id="table_files" cellspacing="0" width="100%" style="font-size: 12px;">

                  <thead>
                      <tr>
                          <th> ID#</th>
                          <th>Fecha</th>
                          <th>Hora</th>
                          <th>Nombre</th>
                          <th>Edad</th>
                          <th>Telefono</th>
                          <th>Rut</th>
                          <th>Estado</th>
                          <th>Acciones</th>
                      </tr>
                  </thead>
                  <tbody>
                    @if($alerts)
                        @forelse($alerts as $alert)
                          <tr>
                              <td>{{ $alert->id }}</td>
                              <td>{{ $alert->created_at }}</td>
                              <td>{{ $alert->created_at }}</td>
                              <td>{{ $alert->person_name }}</td>
                              <td>{{ $alert->person_age }}</td>
                              <td>{{ $alert->person_phone }}</td>
                              <td>{{ $alert->person_dni }}</td>
                              <td>{{ $alert->active == 1 ? 'Activo': 'Inactivo' }}</td>
                              <td>
                                  <a href="/file/" class="btn btn-default btn-xs" data-toggle="tooltip" title="Ver Ficha!" ><i class="fa fa-eye" aria-hidden="true"></i></a>

                              </td>
                          </tr>
                          @empty
                          @endforelse
                  @endif
                  </tbody>
              </table>
              </div>

      </div>
  </section>



<script type="text/javascript">
var geoJsonX;
var table_files;
$(document).ready(function() {


});

$('.datatable').DataTable({
    responsive: true
});

  map = new L.map('divMap');
  var osmUrl='http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png';
  var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
  var osm = new L.TileLayer(osmUrl, {minZoom: 1, maxZoom: 20, attribution: osmAttrib});
  // start the map in South-East England
  map.setView(new L.LatLng(-33.449, -70.669),4);
  map.addLayer(osm);

  function onEachFeature(feature, layer) {
      var popupContent = "<p>Nombre: " + feature.properties.name +
                          "<br>Telefono: " + feature.properties.phone +
                          "<br><a href='/file/"+ feature.properties.id + "'>VER</a></p>";
      layer.bindPopup(popupContent);
  }

  $.getJSON('/api/active_alerts/geojson', function(data){
      geoJsonX = L.geoJSON(data, {onEachFeature: onEachFeature}).addTo(map);
  });

</script>

@endsection
