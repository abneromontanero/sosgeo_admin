@if (Auth::guest())
@else
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Mi Perfil</a></li>
                        <li role="seperator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>Salir</a>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">Menu de Navegacion</li>
                <li class="active">
                    <a href="{{url('/home')}}">
                        <i class="material-icons">home</i>
                        <span>Inicio</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/alerts')}}">
                        <i class="material-icons">pin_drop</i>
                        <span>Alertas</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/reports')}}">
                        <i class="material-icons">poll</i>
                        <span>Reportes</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/users')}}">
                        <i class="material-icons">group</i>
                        <span>Usuarios</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/config')}}">
                        <i class="material-icons">settings</i>
                        <span>Configuracion</span>
                    </a>
                </li>


            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 - 2019 <a href="javascript:void(0);">SosGeo - CSA RM</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    @endif
    <!-- #END# Left Sidebar -->
</section>
