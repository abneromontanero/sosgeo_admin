<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    @include('layouts.admin.css')
</head>
<body class="theme-red">
    <div id="app">
      <div class="overlay"></div>
      @include('layouts.admin.navbar')
      @include('layouts.admin.menu')
      @yield('content')
    </div>
    @include('layouts.admin.js')
</body>
</html>
