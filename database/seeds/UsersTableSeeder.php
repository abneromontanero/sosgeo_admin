<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \DB::table('users')->insert([
    [
      'name' => 'Administrador',
      'position' => 'Super',
      'email' => 'admin@mail.com',
      'password' => bcrypt('adminadmin'),
      'role' => 'superadmin'
    ]

  ]);
    }
}
