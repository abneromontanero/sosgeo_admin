<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->string('resolved')->nullable(); //yes / no
            $table->dateTime('resolved_date')->nullable(); //yes / no
            $table->string('false_alarm')->nullable(); //yes / no
            $table->text('description')->nullable(); //report emergency

            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('altitude')->nullable();

            $table->string('person_name');
            $table->string('person_dni')->nullable();
            $table->integer('person_age')->nullable();
            $table->string('person_phone');
            $table->string('person_emergency_phone')->nullable();
            $table->string('person_gender')->nullable();
            $table->string('person_alergies')->nullable();

            $table->integer('total_group')->nullable();
            $table->integer('total_male')->nullable();
            $table->integer('total_female')->nullable();
            $table->integer('total_deaths')->default(0)->nullable();
            $table->integer('total_minor_injuries')->default(0)->nullable();
            $table->integer('total_serious_injuries')->default(0)->nullable();
            $table->integer('total_uninjuries')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
