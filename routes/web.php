<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/welcome');
});

Auth::routes();

/**Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});**/

Route::group(['middleware' => 'auth'], function () {
  Route::get('/home', function () {
      return view('statistics.index');
  });
  Route::resource('/alerts', 'AlertController');
  Route::resource('/reports', 'ReportController');
  Route::resource('/users', 'UserController');
  Route::resource('/config', 'ConfigController');
});
