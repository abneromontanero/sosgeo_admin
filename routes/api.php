<?php

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

Route::get('/users',function (){
    return \App\User::all();
});

Route::post('/alert',function (Request $request){

    if(isset($request->person_name) && isset($request->person_phone) && isset($request->latitude) && isset($request->longitude))
    {
      $alert = new \App\Alert;
      $alert->person_name = $request->person_name;
      $alert->person_phone = $request->person_phone;
      $alert->latitude = $request->latitude;
      $alert->longitude = $request->longitude;
      $alert->save();
      return 'success data insert';
    }
});

    Route::get('/active_alerts/geojson',function (){

        $active_alerts = \App\Alert::where('active',true)->get();
        $files_geojson = $active_alerts->map(function ($f) {
            return [
                "type"=> "Feature",
                "geometry" =>[
                    "type"=>"Point",
                    "coordinates"=> [
                        $f->longitude,
                        $f->latitude
                    ]
                ],
                "properties"=>[
                    "id"=>$f->id,
                    "name"=>$f->person_name,
                    "phone"=>$f->person_phone
                ]
            ];
        });

        return $files_geojson;
    });


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
