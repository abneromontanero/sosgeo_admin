<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
  protected $fillable = [
      'latitude', 'longitude', 'altitude','person_name','person_dni',
      'person_age','person_phone','person_emergency_phone','person_gender','person_alergies',
      'active'
  ];
}
