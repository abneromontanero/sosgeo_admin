<?php

namespace App\Http\Controllers;

use App\Alert;
use Illuminate\Http\Request;

class AlertController extends Controller
{

    public function index()
    {
      $active_alerts = \App\Alert::where('active',true)->get();
      return view('alerts.index')->with('alerts', $active_alerts);
    }


    public function create()
    {
        return view('alerts.create');
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Alert $alert)
    {
        return view('alerts.show')->with('alert', $alert);
    }


    public function edit(Alert $alert)
    {
        return view('alerts.edit')->with('alert', $alert);
    }


    public function update(Request $request, Alert $alert)
    {
        //
    }


    public function destroy(Alert $alert)
    {
        //
    }
}
